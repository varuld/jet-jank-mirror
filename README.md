# Demonstration program for Header library for Waveshare Jetank running on Jetson Nano

![](./robot.gif)


# !NOTICE!
This a mirror/fork of the project, which is originally hosted on github

## Table of Contents

- [Requirements](#requirements)
- [Setup](#setup)
- [Usage](#usage)
- [Program Description](#program-description-and-assumptions)
- [Maintainers](#maintainers)
- [Future plans for the project](#future-plans-for-the-project)
- [Known issues](#known-issues)


## Usage
This program is intended to provide a low latency interface to the Waveshare Jetank robot on the Nvidia Jetson Nano, by using C++ to control the movement.
This is done through a CLI interface, such that you a able to control the robot over ssh etc.

## Program Description and assumptions
The motor interaction are provided as headers files in ![](./Jetank) folder and the ![](./demo.cpp) file is provide as a demonstration to the capability of the library. The demo program can be compiled via CMake. An automated build script for this demo program is included (demo_launch).

## Requirements

Requires `gcc` and `cmake`.

## Setup

	$ run the `demo_launch` script

	---

	OR;

	---

    $ mkdir build
    $ cd build
    $ cmake ../
    $ make
    $ ./main

## Maintainers
- Ole Kjepso [@olemikole](https://github.com/olemikole)
- Christian Danø [@varuld](https://github.com/varuld)
- Tellef Østereng [@tellefo92](https://github.com/tellefo92)

## Future plans for the project
Extending the functionally by providing camera access along with rewriting the arm controller to use the I2C protocol is the logical steps to better the project.

## Known issues
- demo program must be runned as root or with sudo privileges, OR the "/dev/ttyTHS1" must be changed to 666 through "sudo chmod 666 /dev/ttyTHS1"
